import pytest
import requests
import read_excel
from yaml_unil import read_yaml


@pytest.fixture(scope="session")
def session():
    return requests.session()


@pytest.fixture(scope="session")
def user_session(session):
    """登录"""
    data = {"mobile": "19514762825",
            "verification_code": "888888"
            }
    resp = session.post("http://test.admin.xyz.moinai.com/user/_login", json=data)

    # 为session设置请求头，一次设置，后面的用例全部有效
    session.headers["Authorization"] = "Bearer " + resp.json()["token"]

    return session


@pytest.mark.parametrize('args',read_yaml())
def test_new_linkman(args):
    """
    数据驱动方法实现新增联系人
    """
    url = args['api_request']['url']
    method = args['api_request']['method']
    headers = args['api_request']['headers']
    params = args['api_request']['params']

    if method == 'get':
        requests.get()        # 如果method是get就调用get方法
    else:
        resp=requests.post(url, json=params, headers=headers)
        print(resp.json())

    assert resp.status_code == 201

    return resp.json()


@pytest.mark.parametrize('args',read_yaml())
def test_create_room(args):
    """"创建房间"""
    url = args['api_create']['url']
    method = args['api_create']['method']
    headers = args['api_create']['headers']
    params = args['api_create']['params']

    if method == 'get':
        requests.get()
    else:
        resp = requests.post(url, json=params, headers=headers)
        print(resp.json())

    assert resp.status_code == 201

    return resp.json()


@pytest.mark.parametrize('args',read_yaml())
def test_room_template(args):
    """"保存为模版"""
    url = args['api_template']['url']
    method = args['api_template']['method']
    headers = args['api_template']['headers']
    params = args['api_template']['params']

    if method == 'get':
        requests.get()
    else:
        resp = requests.post(url, json=params, headers=headers)
        print(resp.json())

    assert resp.status_code == 201

    return resp.json()


@pytest.fixture    # 为修改联系人用例单独使用
def contact_id(user_session):
    """新增联系人"""
    data = {"name": "接口自大户测试调试",
            "mobile": "18576382243",
            "client_id": "630f0a183f0efb9c6aa91df0",
            "company_id": "62bfa1f8462d254fd6e0181a",}
    resp = user_session.post("http://test.admin.xyz.moinai.com/contact?company_id=62bfa1f8462d254fd6e0181a",
                       json=data)

    assert resp.status_code == 201  # 状态码返回

    return resp.json()['_id']      # 返回新建联系人的id


@pytest.mark.parametrize("mobile,pswd,code,mark",read_excel.read_excel_1())
def test_login(mobile, pswd, code, mark):
    """参数化登录"""
    data = {"mobile": mobile,
            "verification_code": pswd,}
    resp = requests.post("http://test.admin.xyz.moinai.com/user/_login",
                           json=data)
    assert resp.status_code == code


def test_staff(user_session):
    """公司员工列表"""
    resp = user_session.get(
        "http://test.admin.xyz.moinai.com/staff?company_id=62bfa1f8462d254fd6e0181a",
                       )
    assert resp.status_code == 200   # 状态码返回


def test_staff_count(user_session):
    """"员工列表count"""
    resp = user_session.get(
        "http://test.admin.xyz.moinai.com/staff/_count?company_id=62bfa1f8462d254fd6e0181a",
                         )
    assert resp.status_code == 200   # 状态码返回


def test_contact_update(user_session,contact_id):    # 修改联系人接口调用test_contcat新建联系人接口
    """修改联系人"""
    # 需要用到_id 来构建完整的url
    resp = user_session.patch(
        f"http://test.admin.xyz.moinai.com/contact/{contact_id}?company_id=62bfa1f8462d254fd6e0181a",
        json={"name":"接口自动化调试", "client_id":"630f0a183f0efb9c6aa91df0"},
        )
    assert resp.status_code == 200  # 状态码返回


@pytest.mark.repeat(1)
def test_content_upload(user_session):
    """上传文件"""
    url = 'http://test.admin.xyz.moinai.com/content?company_id=62bfa1f8462d254fd6e0181a'  # 上传文件接口
    file_data = {'file': ('上传1件.pdf', open('test_case_api/文件上传.pdf', 'rb'),
                          'pdf/jpeg/png'  # 文件类型
                          )}
    data = {'category':'62bfa1f8462d254fd6e01823',
            'library_id':'62bfa1f8462d254fd6e0181d',
            'title':'测试环境ba1212',}    # 文件名称
    resp = user_session.post(url=url, files=file_data, data=data)
    print(resp.json())
    assert resp.status_code == 201
