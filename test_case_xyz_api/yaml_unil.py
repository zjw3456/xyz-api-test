import yaml


def read_yaml():
    """
    读取yaml文件
    :return:
    """
    with open('test_case_api/api_new.yaml', encoding='utf-8') as f:
        data = yaml.load(f,Loader=yaml.FullLoader)
        return data


if __name__ == '__main__':
    print(read_yaml())
