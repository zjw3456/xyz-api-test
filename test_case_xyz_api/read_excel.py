from openpyxl import load_workbook
# 需要下载openpyxl库，使用load_workbook来读


def read_excel_1():
   """
   登录接口使用
   """
   wb = load_workbook("test_case_api/登录接口自动化.xlsx")
   ws = wb.active   # 默认sheet

   return list(ws.values)[1:]
   # values定义为一个列表