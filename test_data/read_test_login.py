from openpyxl import load_workbook
import os

path = os.path.dirname(os.path.realpath(__file__))


def read_excel_login():
    """
    摹因测试环境登录接口专用
    """
    wb = load_workbook(path+"/摹因测试环境登录.xlsx")
    ws = wb.active  # 默认sheet

    return list(ws.values)[1:]

