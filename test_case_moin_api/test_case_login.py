import allure
import pytest
import requests
import sys
sys.path.append("..")
import test_data.read_test_login as read_test_login
import test_data.read_official_login as read_official_login


@pytest.fixture(scope='session')
def session():
    return requests.session()


@pytest.fixture(scope="session")
def user_session(session):
    """登录"""
    data = {"account": "eftest",
            "pass": "eftest1"}
    resp = session.post("http://test.api.moinai.com/user/login", json=data)

    # 为session设置请求头，一次设置，后面的用例全部有效
    session.headers["Authorization"] = resp.json()["token"]

    return session


@allure.feature('正式环境参数化登录')
@pytest.mark.parametrize("account,pswd,code,mark",read_test_login.read_excel_login())
def test_login(account, pswd, code, mark):
    """正式环境参数化登录"""
    headers = {"Content-Type": "application/json; charset=utf-8"}
    data = {
        "account": str(account),
        "pass": str(pswd)}
    resp = requests.post("http://test.api.moinai.com/user/login", json=data, headers=headers)
    assert resp.status_code == code
    print(resp.json())


@allure.story("正式环境参数化登录")
@pytest.mark.parametrize("account,pswd,code,mark",read_official_login.read_excel_login())
def test_official_login(account, pswd, code, mark):
    """正式环境参数化登录"""
    headers = {"Content-Type": "application/json; charset=utf-8"}
    data = {
        "account": str(account),
         "pass": str(pswd)}
    resp = requests.post("http://api.moinai.com/user/login", json=data, headers=headers)
    assert resp.status_code == code
    print(resp.json())

